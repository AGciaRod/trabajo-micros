/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.c
  * @brief          : Main program body
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2021 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under BSD 3-Clause license,
  * the "License"; You may not use this file except in compliance with the
  * License. You may obtain a copy of the License at:
  *                        opensource.org/licenses/BSD-3-Clause
  *
  ******************************************************************************
  */
/* USER CODE END Header */
/* Includes ------------------------------------------------------------------*/
#include "main.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include <stdlib.h>
/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */

/*----------- Botones -----------*/
uint16_t PIN_BSalon    =   GPIO_PIN_2; // Asignación BOTON SALON
uint16_t PIN_BComedor  =   GPIO_PIN_3; // Asignación BOTON COMEDOR


/*----------- LEDS -----------*/
uint16_t PIN_LSalon1   =   GPIO_PIN_10; // Asignación LED SALON 1
uint16_t PIN_LSalon2   =   GPIO_PIN_11; // Asignación LED SALON 2
uint16_t PIN_LSalon3   =   GPIO_PIN_12; // Asignación LED SALON 3

uint16_t PIN_LComedor1 =   GPIO_PIN_13; // Asignación LED COMEDOR 1
uint16_t PIN_LComedor2 =   GPIO_PIN_14; // Asignación LED COMEDOR 2
uint16_t PIN_LComedor3 =   GPIO_PIN_15; // Asignación LED COMEDOR 3

/*------SENSORES DE ULTRASONIDOS---------*/
uint16_t PIN_TrigSalon    =   GPIO_PIN_4;
uint16_t PIN_EchoSalon    =   GPIO_PIN_5;
uint16_t PIN_TrigComedor  =   GPIO_PIN_7;
uint16_t PIN_EchoComedor  =   GPIO_PIN_8;


/*----------- Máquina de Estados -----------*/
enum estados{AutSalAutCom, AutSalManCom , ManSalAutCom, ManSalManCom };

/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */
/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/
ADC_HandleTypeDef hadc1;

/* USER CODE BEGIN PV */
ADC_HandleTypeDef hadc1;

/*----------- Estado Inicial Máquina de Estados -----------*/
volatile int estado = AutSalAutCom;

/*----------- Estado Inicial Botones -----------*/
volatile int BComedor=0;
volatile int BSalon=0;

/*----------- Control de Rebotes -----------*/
const int timeThreshold = 100;
long startTime = 0;


/*-------------SENSORES----------*/
uint16_t Valor_LDR = 0;




/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
static void MX_GPIO_Init(void);
static void MX_ADC1_Init(void);
/* USER CODE BEGIN PFP */

/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */

//LDR
void HAL_ADC_ConvCpltCallback(ADC_HandleTypeDef* hadc)
{
	if (hadc->Instance == ADC1){
		Valor_LDR = HAL_ADC_GetValue(&hadc1);}
}


/*------------AUXILIARES----------------*/
int auxComedor=0, auxSalon=0;
int ProfundidadSalon=20, ProfundidadComedor=20;
int DistanciaSalon=0,DistanciaComedor=0;
/*----------- Control de Rebotes -----------*/
void HAL_GPIO_EXTI_Callback(uint16_t GPIO_Pin)
{
	if (GPIO_Pin==PIN_BComedor){BComedor=1;	}
	else if (GPIO_Pin==PIN_BSalon){BSalon=1;}
}

int debouncer(volatile int* button_int, GPIO_TypeDef* GPIO_port, uint16_t GPIO_number) // Control de los rebotes
{
	static uint8_t button_count=0;
	static int counter=0;

	if (*button_int==1)	{
		if (button_count==0)		{
			counter=HAL_GetTick();
			button_count++;
		}
		if (HAL_GetTick()-counter>=20)		{
			counter=HAL_GetTick();
			if (HAL_GPIO_ReadPin(GPIO_port, GPIO_number)!=1){
				button_count=1;
			}
			else{
				button_count++;
			}
			if (button_count==4 ) // Periodo antirebotes
			{
				button_count=0;
				*button_int=0;
				return 1;
			}
		}
	}
	return 0;
}

int LecturaSalon(void){
	int AuxTiempoSalon=0, DisSalon=ProfundidadSalon;
	HAL_GPIO_WritePin(GPIOE,PIN_TrigSalon,GPIO_PIN_RESET);
	HAL_Delay(2);
	HAL_GPIO_WritePin(GPIOE,PIN_TrigSalon,GPIO_PIN_SET);
	HAL_Delay(10);
	HAL_GPIO_WritePin(GPIOE,PIN_TrigSalon,GPIO_PIN_RESET);
	while(HAL_GPIO_ReadPin(GPIOE,PIN_EchoSalon)){AuxTiempoSalon++;}
	DisSalon=AuxTiempoSalon/(29.2*2);
	return DisSalon;
}
int LecturaComedor(void){
	int AuxTiempoComedor=0, DisComedor=ProfundidadComedor;
	HAL_GPIO_WritePin(GPIOE,PIN_TrigComedor,GPIO_PIN_RESET);
	HAL_Delay(2);
	HAL_GPIO_WritePin(GPIOE,PIN_TrigComedor,GPIO_PIN_SET);
	HAL_Delay(10);
	HAL_GPIO_WritePin(GPIOE,PIN_TrigComedor,GPIO_PIN_RESET);
	while(HAL_GPIO_ReadPin(GPIOE,PIN_EchoComedor)){AuxTiempoComedor++;}
	DisComedor=AuxTiempoComedor/(29.2*2);
	return DisComedor;
}
//----------- Funciones de los Actuadores -----------
void LucesManualesComedor(void){
	 if ( auxComedor==2){
		HAL_GPIO_WritePin(GPIOE, PIN_LComedor1, 1);
		HAL_GPIO_WritePin(GPIOE, PIN_LComedor2, 1);
		HAL_GPIO_WritePin(GPIOE, PIN_LComedor3, 1);}
	 else if (auxComedor==1){
		HAL_GPIO_WritePin(GPIOE, PIN_LComedor1, 0);
		HAL_GPIO_WritePin(GPIOE, PIN_LComedor2, 0);
		HAL_GPIO_WritePin(GPIOE, PIN_LComedor3, 0);}
}

void LucesManualesSalon(void){
	 if ( auxSalon==2) {
		HAL_GPIO_WritePin(GPIOE, PIN_LSalon1, 1);
		HAL_GPIO_WritePin(GPIOE, PIN_LSalon2, 1);
		HAL_GPIO_WritePin(GPIOE, PIN_LSalon3, 1);}
	 else if (auxSalon==1){
		HAL_GPIO_WritePin(GPIOE, PIN_LSalon1, 0);
		HAL_GPIO_WritePin(GPIOE, PIN_LSalon2, 0);
		HAL_GPIO_WritePin(GPIOE, PIN_LSalon3, 0);}
}


void LucesAutomaticasSalon(void){
 if (DistanciaSalon>0 && DistanciaSalon<ProfundidadSalon){
	if      (Valor_LDR>0 && Valor_LDR<64){
		HAL_GPIO_WritePin(GPIOE, PIN_LSalon1, 1);
		HAL_GPIO_WritePin(GPIOE, PIN_LSalon2, 1);
		HAL_GPIO_WritePin(GPIOE, PIN_LSalon3, 1);}
	else if (Valor_LDR>64 && Valor_LDR<128){
    	HAL_GPIO_WritePin(GPIOE, PIN_LSalon1, 1);
		HAL_GPIO_WritePin(GPIOE, PIN_LSalon2, 1);
		HAL_GPIO_WritePin(GPIOE, PIN_LSalon3, 0);}
	else if (Valor_LDR>128 && Valor_LDR<192){
		HAL_GPIO_WritePin(GPIOE, PIN_LSalon1, 1);
		HAL_GPIO_WritePin(GPIOE, PIN_LSalon2, 0);
		HAL_GPIO_WritePin(GPIOE, PIN_LSalon3, 0);}
	else if (Valor_LDR>192 && Valor_LDR<255){
		HAL_GPIO_WritePin(GPIOE, PIN_LSalon1, 0);
		HAL_GPIO_WritePin(GPIOE, PIN_LSalon2, 0);
		HAL_GPIO_WritePin(GPIOE, PIN_LSalon3, 0);}}
 else{ HAL_GPIO_WritePin(GPIOE, PIN_LSalon1, 0);
	   HAL_GPIO_WritePin(GPIOE, PIN_LSalon2, 1);//PONER A 0 CUANDO FUNCIONE
	   HAL_GPIO_WritePin(GPIOE, PIN_LSalon3, 0);}
}
void LucesAutomaticasComedor(void){
 if (DistanciaComedor>0 && DistanciaComedor<ProfundidadComedor){
	if      (Valor_LDR>0 && Valor_LDR<64){
		HAL_GPIO_WritePin(GPIOE, PIN_LComedor1, 1);
		HAL_GPIO_WritePin(GPIOE, PIN_LComedor2, 1);
		HAL_GPIO_WritePin(GPIOE, PIN_LComedor3, 1);}
	else if (Valor_LDR>64 && Valor_LDR<128){
		HAL_GPIO_WritePin(GPIOE, PIN_LComedor1, 1);
		HAL_GPIO_WritePin(GPIOE, PIN_LComedor2, 1);
		HAL_GPIO_WritePin(GPIOE, PIN_LComedor3, 0);}
	else if (Valor_LDR>128 && Valor_LDR<192){
		HAL_GPIO_WritePin(GPIOE, PIN_LComedor1, 1);
		HAL_GPIO_WritePin(GPIOE, PIN_LComedor2, 0);
		HAL_GPIO_WritePin(GPIOE, PIN_LComedor3, 0);}
	else if (Valor_LDR>192 && Valor_LDR<255){
		HAL_GPIO_WritePin(GPIOE, PIN_LComedor1, 0);
		HAL_GPIO_WritePin(GPIOE, PIN_LComedor2, 0);
		HAL_GPIO_WritePin(GPIOE, PIN_LComedor3, 0);}}
 else {HAL_GPIO_WritePin(GPIOE, PIN_LComedor1, 0);
	   HAL_GPIO_WritePin(GPIOE, PIN_LComedor2, 1); //PONER A 0 CUANDO FUNCIONE
	   HAL_GPIO_WritePin(GPIOE, PIN_LComedor3, 0);}
}


/* USER CODE END 0 */

/**
  * @brief  The application entry point.
  * @retval int
  */
int main(void)
{
  /* USER CODE BEGIN 1 */

  /* USER CODE END 1 */

  /* MCU Configuration--------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();

  /* USER CODE BEGIN Init */

  /* USER CODE END Init */

  /* Configure the system clock */
  SystemClock_Config();

  /* USER CODE BEGIN SysInit */

  /* USER CODE END SysInit */

  /* Initialize all configured peripherals */
  MX_GPIO_Init();
  MX_ADC1_Init();
  /* USER CODE BEGIN 2 */


  /* USER CODE END 2 */

  /* Infinite loop */
  /* USER CODE BEGIN WHILE */
  while (1)
  {
    /* USER CODE END WHILE */

    /* USER CODE BEGIN 3 */
	  //LECTURA LDR
	  HAL_ADC_Start_IT(&hadc1);
	  HAL_ADC_ConvCpltCallback(&hadc1);

	  //LECTURA SENSORES ULTRASONIDOS
	  DistanciaSalon=LecturaSalon();
	  DistanciaComedor=LecturaComedor();

	/*-----------CAMBIO DE ESTADO----------*/
	  if (debouncer(&BComedor, GPIOE, PIN_BComedor)){
	  		if 			(estado == AutSalAutCom && auxComedor==0) 	{estado = AutSalManCom; auxComedor=2;}
	  		else if 	(estado == AutSalManCom && auxComedor==2) 	{estado = AutSalManCom; auxComedor=1;}
	  	  	else if 	(estado == AutSalManCom && auxComedor==1) 	{estado = AutSalAutCom; auxComedor=0;}
	  	  	else if 	(estado == ManSalAutCom && auxComedor==0) 	{estado = ManSalManCom; auxComedor=2;}
	  	    else if 	(estado == ManSalManCom && auxComedor==2) 	{estado = ManSalManCom; auxComedor=1;}
	  		else if 	(estado == ManSalManCom && auxComedor==1) 	{estado = ManSalAutCom; auxComedor=0;}

	  	  	}

	  if (debouncer(&BSalon, GPIOE, PIN_BSalon)){
	  		if 		    (estado == AutSalAutCom && auxSalon==0) 	{estado = ManSalAutCom; auxSalon=2;}
	  		else if 	(estado == ManSalAutCom && auxSalon==2) 	{estado = ManSalAutCom; auxSalon=1;}
	  		else if 	(estado == ManSalAutCom && auxSalon==1) 	{estado = AutSalAutCom; auxSalon=0;}
	  		else if 	(estado == AutSalManCom && auxSalon==0) 	{estado = ManSalManCom; auxSalon=2;}
	  		else if 	(estado == ManSalManCom && auxSalon==2) 	{estado = ManSalManCom; auxSalon=1;}
	  		else if 	(estado == ManSalManCom && auxSalon==1) 	{estado = AutSalManCom; auxSalon=0;}
	  		}

	/*--------------- Funciones de Estados ---------------*/
	   switch (estado){
	  	  case AutSalAutCom: LucesAutomaticasSalon(); LucesAutomaticasComedor();  break;
	  	  case AutSalManCom: LucesAutomaticasSalon(); LucesManualesComedor();	  break;
	  	  case ManSalAutCom: LucesManualesSalon();    LucesAutomaticasComedor();  break;
	  	  case ManSalManCom: LucesManualesSalon();    LucesManualesComedor();	  break;
	      }
  }
  /* USER CODE END 3 */
}

/**
  * @brief System Clock Configuration
  * @retval None
  */
void SystemClock_Config(void)
{
  RCC_OscInitTypeDef RCC_OscInitStruct = {0};
  RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};

  /** Configure the main internal regulator output voltage
  */
  __HAL_RCC_PWR_CLK_ENABLE();
  __HAL_PWR_VOLTAGESCALING_CONFIG(PWR_REGULATOR_VOLTAGE_SCALE1);
  /** Initializes the RCC Oscillators according to the specified parameters
  * in the RCC_OscInitTypeDef structure.
  */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSI;
  RCC_OscInitStruct.HSIState = RCC_HSI_ON;
  RCC_OscInitStruct.HSICalibrationValue = RCC_HSICALIBRATION_DEFAULT;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSI;
  RCC_OscInitStruct.PLL.PLLM = 8;
  RCC_OscInitStruct.PLL.PLLN = 50;
  RCC_OscInitStruct.PLL.PLLP = RCC_PLLP_DIV2;
  RCC_OscInitStruct.PLL.PLLQ = 4;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    Error_Handler();
  }
  /** Initializes the CPU, AHB and APB buses clocks
  */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV2;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV1;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_1) != HAL_OK)
  {
    Error_Handler();
  }
}

/**
  * @brief ADC1 Initialization Function
  * @param None
  * @retval None
  */
static void MX_ADC1_Init(void)
{

  /* USER CODE BEGIN ADC1_Init 0 */

  /* USER CODE END ADC1_Init 0 */

  ADC_ChannelConfTypeDef sConfig = {0};

  /* USER CODE BEGIN ADC1_Init 1 */

  /* USER CODE END ADC1_Init 1 */
  /** Configure the global features of the ADC (Clock, Resolution, Data Alignment and number of conversion)
  */
  hadc1.Instance = ADC1;
  hadc1.Init.ClockPrescaler = ADC_CLOCK_SYNC_PCLK_DIV2;
  hadc1.Init.Resolution = ADC_RESOLUTION_8B;
  hadc1.Init.ScanConvMode = DISABLE;
  hadc1.Init.ContinuousConvMode = DISABLE;
  hadc1.Init.DiscontinuousConvMode = DISABLE;
  hadc1.Init.ExternalTrigConvEdge = ADC_EXTERNALTRIGCONVEDGE_NONE;
  hadc1.Init.ExternalTrigConv = ADC_SOFTWARE_START;
  hadc1.Init.DataAlign = ADC_DATAALIGN_RIGHT;
  hadc1.Init.NbrOfConversion = 1;
  hadc1.Init.DMAContinuousRequests = DISABLE;
  hadc1.Init.EOCSelection = ADC_EOC_SINGLE_CONV;
  if (HAL_ADC_Init(&hadc1) != HAL_OK)
  {
    Error_Handler();
  }
  /** Configure for the selected ADC regular channel its corresponding rank in the sequencer and its sample time.
  */
  sConfig.Channel = ADC_CHANNEL_1;
  sConfig.Rank = 1;
  sConfig.SamplingTime = ADC_SAMPLETIME_3CYCLES;
  if (HAL_ADC_ConfigChannel(&hadc1, &sConfig) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN ADC1_Init 2 */

  /* USER CODE END ADC1_Init 2 */

}

/**
  * @brief GPIO Initialization Function
  * @param None
  * @retval None
  */
static void MX_GPIO_Init(void)
{
  GPIO_InitTypeDef GPIO_InitStruct = {0};

  /* GPIO Ports Clock Enable */
  __HAL_RCC_GPIOE_CLK_ENABLE();
  __HAL_RCC_GPIOH_CLK_ENABLE();
  __HAL_RCC_GPIOA_CLK_ENABLE();
  __HAL_RCC_GPIOD_CLK_ENABLE();
  __HAL_RCC_GPIOC_CLK_ENABLE();

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOE, GPIO_PIN_10|GPIO_PIN_11|GPIO_PIN_12|GPIO_PIN_13
                          |GPIO_PIN_14|GPIO_PIN_15, GPIO_PIN_RESET);

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOD, GPIO_PIN_12|GPIO_PIN_13|GPIO_PIN_14|GPIO_PIN_15, GPIO_PIN_RESET);

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOC, GPIO_PIN_6|GPIO_PIN_8, GPIO_PIN_RESET);

  /*Configure GPIO pins : PE2 PE3 */
  GPIO_InitStruct.Pin = GPIO_PIN_2|GPIO_PIN_3;
  GPIO_InitStruct.Mode = GPIO_MODE_IT_FALLING;
  GPIO_InitStruct.Pull = GPIO_PULLUP;
  HAL_GPIO_Init(GPIOE, &GPIO_InitStruct);

  /*Configure GPIO pins : PE10 PE11 PE12 PE13
                           PE14 PE15 */
  GPIO_InitStruct.Pin = GPIO_PIN_10|GPIO_PIN_11|GPIO_PIN_12|GPIO_PIN_13
                          |GPIO_PIN_14|GPIO_PIN_15;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(GPIOE, &GPIO_InitStruct);

  /*Configure GPIO pins : PD12 PD13 PD14 PD15 */
  GPIO_InitStruct.Pin = GPIO_PIN_12|GPIO_PIN_13|GPIO_PIN_14|GPIO_PIN_15;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(GPIOD, &GPIO_InitStruct);

  /*Configure GPIO pins : PC6 PC8 */
  GPIO_InitStruct.Pin = GPIO_PIN_6|GPIO_PIN_8;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(GPIOC, &GPIO_InitStruct);

  /*Configure GPIO pins : PC7 PC9 */
  GPIO_InitStruct.Pin = GPIO_PIN_7|GPIO_PIN_9;
  GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  HAL_GPIO_Init(GPIOC, &GPIO_InitStruct);

  /* EXTI interrupt init*/
  HAL_NVIC_SetPriority(EXTI2_IRQn, 0, 0);
  HAL_NVIC_EnableIRQ(EXTI2_IRQn);

  HAL_NVIC_SetPriority(EXTI3_IRQn, 0, 0);
  HAL_NVIC_EnableIRQ(EXTI3_IRQn);

}

/* USER CODE BEGIN 4 */

/* USER CODE END 4 */

/**
  * @brief  This function is executed in case of error occurrence.
  * @retval None
  */
void Error_Handler(void)
{
  /* USER CODE BEGIN Error_Handler_Debug */
  /* User can add his own implementation to report the HAL error return state */

  /* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t *file, uint32_t line)
{
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
     tex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
